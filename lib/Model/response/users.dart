import 'package:cloud_firestore/cloud_firestore.dart';

/// FCMToken : "e5LWaHYqRKqfHaTwnapoG2:APA91bFCQdXiwfXpJTS1OJAcXIlbtcg_xPAH8yGYueLS1WivFaUZIwG-Fc5LJl56Xn_AvT9JCBcNF9mHO97nkDdoS6XkvNU1JbqtMufJoeAS_e8K27beS5-maLAbF7o19EqwJJgHqEjf"
/// createdAt : 1602156296092
/// intro : "Khan2"
/// name : "Jannat2"
/// userId : "Jannat13325"
/// userImageUrl : "https://firebasestorage.googleapis.com/v0/b/chattestapp-bdbdc.appspot.com/o/userImages%2FJannat64786?alt=media&token=823f164a-d1a4-4b56-835a-6df29f67e5da"

class UserSave {
  String _fCMToken;
  FieldValue _createdAt;
  String _intro;
  String _name;
  String _userId;
  String _userImageUrl;

  String get fCMToken => _fCMToken;
  FieldValue get createdAt => _createdAt;
  String get intro => _intro;
  String get name => _name;
  String get userId => _userId;
  String get userImageUrl => _userImageUrl;

  UserSave({
    String fCMToken,
    FieldValue createdAt,
    String intro,
    String name,
    String userId,
    String userImageUrl}){
    _fCMToken = fCMToken;
    _createdAt = createdAt;
    _intro = intro;
    _name = name;
    _userId = userId;
    _userImageUrl = userImageUrl;
  }

  UserSave.fromJson(dynamic json) {
    _fCMToken = json["FCMToken"];
    _createdAt = json["createdAt"];
    _intro = json["intro"];
    _name = json["name"];
    _userId = json["userId"];
    _userImageUrl = json["userImageUrl"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["FCMToken"] = _fCMToken;
    map["createdAt"] = _createdAt;
    map["intro"] = _intro;
    map["name"] = _name;
    map["userId"] = _userId;
    map["userImageUrl"] = _userImageUrl;
    return map;
  }

}

class UserRetrieve {
  String _fCMToken;
  Timestamp _createdAt;
  String _intro;
  String _name;
  String _userId;
  String _userImageUrl;

  String get fCMToken => _fCMToken;
  Timestamp get createdAt => _createdAt;
  String get intro => _intro;
  String get name => _name;
  String get userId => _userId;
  String get userImageUrl => _userImageUrl;

  UserRetrieve({
    String fCMToken,
    Timestamp createdAt,
    String intro,
    String name,
    String userId,
    String userImageUrl}){
    _fCMToken = fCMToken;
    _createdAt = createdAt;
    _intro = intro;
    _name = name;
    _userId = userId;
    _userImageUrl = userImageUrl;
  }

  UserRetrieve.fromJson(dynamic json) {
    _fCMToken = json["FCMToken"];
    _createdAt = json["createdAt"];
    _intro = json["intro"];
    _name = json["name"];
    _userId = json["userId"];
    _userImageUrl = json["userImageUrl"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["FCMToken"] = _fCMToken;
    map["createdAt"] = _createdAt;
    map["intro"] = _intro;
    map["name"] = _name;
    map["userId"] = _userId;
    map["userImageUrl"] = _userImageUrl;
    return map;
  }

}