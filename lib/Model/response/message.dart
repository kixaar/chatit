import 'package:cloud_firestore/cloud_firestore.dart';

/// datetime : "2020-02-21 10:37:26"
/// message : "How are tou?"
/// msgseen : false
/// receiver : "-M0aOtWwaq5QpKgqq-AL"
/// sender : "slkiBADfBCfaADhbg9PVeH2wpGH2"
/// type : "TEXT"

class MessageSend {
  FieldValue _datetime;
  String _message;
  String _messageId;
  bool _messageStarred;
  bool _msgseen;
  String _receiver;
  String _senderID;
  String _senderName;
  String _type;

  FieldValue get datetime => _datetime;
  String get message => _message;
  String get messageId => _messageId;
  bool get messageStarred => _messageStarred;
  bool get msgseen => _msgseen;
  String get receiver => _receiver;
  String get senderID => _senderID;
  String get senderName => _senderName;
  String get type => _type;

  MessageSend({
    FieldValue datetime,
    String messageId,
    String message,
    bool messageStarred,
      bool msgseen, 
      String receiver, 
      String senderID,
      String senderName,
      String type}){
    _datetime = datetime;
    _messageId = messageId;
    _message = message;
    _messageStarred= messageStarred;
    _msgseen = msgseen;
    _receiver = receiver;
    _senderID = senderID;
    _senderName = senderName;
    _type = type;
}

  MessageSend.fromJson(dynamic json) {
    _datetime = json["datetime"];
    _messageId = json["messageId"];
    _message = json["message"];
    _messageStarred = json["messageStarred"];
    _msgseen = json["msgseen"];
    _receiver = json["receiver"];
    _senderID = json["senderID"];
    _senderName = json["senderName"];
    _type = json["type"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["datetime"] = _datetime;
    map["messageId"] = _messageId;
    map["message"] = _message;
    map["messageStarred"] = _messageStarred;
    map["msgseen"] = _msgseen;
    map["receiver"] = _receiver;
    map["senderID"] = _senderID;
    map["senderName"] = _senderName;
    map["type"] = _type;
    return map;
  }

}

class MessageReceive {
  Timestamp _datetime;
  String _message;
  String _messageId;
  bool _messageStarred;
  bool _msgseen;
  String _receiver;
  String _senderID;
  String _senderName;
  String _type;

  Timestamp get datetime => _datetime;
  String get message => _message;
  String get messageId => _messageId;
  bool get messageStarred => _messageStarred;
  bool get msgseen => _msgseen;
  String get receiver => _receiver;
  String get senderID => _senderID;
  String get senderName => _senderName;
  String get type => _type;

  MessageReceive({
    Timestamp datetime,
    String messageId,
    String message,
    bool messageStarred,
    bool msgseen,
    String receiver,
    String senderID,
    String senderName,
    String type}){
    _datetime = datetime;
    _messageId = messageId;
    _message = message;
    _messageStarred= messageStarred;
    _msgseen = msgseen;
    _receiver = receiver;
    _senderID = senderID;
    _senderName = senderName;
    _type = type;
  }

  MessageReceive.fromJson(dynamic json) {
    _datetime = json["datetime"];
    _messageId = json["messageId"];
    _message = json["message"];
    _messageStarred = json["messageStarred"];
    _msgseen = json["msgseen"];
    _receiver = json["receiver"];
    _senderID = json["senderID"];
    _senderName = json["senderName"];
    _type = json["type"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["datetime"] = _datetime;
    map["messageId"] = _messageId;
    map["message"] = _message;
    map["messageStarred"] = _messageStarred;
    map["msgseen"] = _msgseen;
    map["receiver"] = _receiver;
    map["senderID"] = _senderID;
    map["senderName"] = _senderName;
    map["type"] = _type;
    return map;
  }

}


