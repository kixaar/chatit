
class UsersList{
  String name;
  String userImageUrl;
  String backdropPhoto;
  String location;
  String logo;
  String president;

  UsersList.fromDb(dynamic data) {
    name = data['name'];
    userImageUrl = data['about'];
    backdropPhoto = data['backdropPhoto'];
    location = data['location'];
    logo = data['logo'];
    president = data['president'];
  }
}