import 'package:cloud_firestore/cloud_firestore.dart';

/// active : true
/// admin : "slkiBADfBCfaADhbg9PVeH2wpGH2"
/// createdAt : "2020-02-21 06:20:31"
/// groupImg : "https://firebasestorage.googleapis.com/v0/b/sta..."
/// groupName : "Android Team"
/// id : "-M0aOtWwaq5QpKgqq-AL"
/// lastMsg : "H7h7"
/// lastMsgTime : "2020-02-21 10:38:12"
/// members : [{"0":"slkiBADfBCfaADhbg9PVeH2wpGH2"}]
/// type : "TEXT"

class GroupSave {
  bool _active;
  String _admin;
  FieldValue _createdAt;
  String _groupImg;
  String _groupName;
  String _id;
  String _lastMsg;
  String _lastMsgTime;
  List<String> _members;
  String _type;

  bool get active => _active;
  String get admin => _admin;
  FieldValue get createdAt => _createdAt;
  String get groupImg => _groupImg;
  String get groupName => _groupName;
  String get id => _id;
  String get lastMsg => _lastMsg;
  String get lastMsgTime => _lastMsgTime;
  List<String> get members => _members;
  String get type => _type;

  GroupSave({
      bool active, 
      String admin, 
      FieldValue createdAt,
      String groupImg, 
      String groupName, 
      String id, 
      String lastMsg, 
      String lastMsgTime, 
      List<String> members,
      String type}){
    _active = active;
    _admin = admin;
    _createdAt = createdAt;
    _groupImg = groupImg;
    _groupName = groupName;
    _id = id;
    _lastMsg = lastMsg;
    _lastMsgTime = lastMsgTime;
    _members = members;
    _type = type;
}

  GroupSave.fromJson(dynamic json) {
    _active = json["active"];
    _admin = json["admin"];
    _createdAt = json["createdAt"];
    _groupImg = json["groupImg"];
    _groupName = json["groupName"];
    _id = json["id"];
    _lastMsg = json["lastMsg"];
    _lastMsgTime = json["lastMsgTime"];
    if (json["members"] != null) {
      _members = [];
      json["members"].forEach((v) {
        _members.add(v);
      });
    }
    _type = json["type"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["active"] = _active;
    map["admin"] = _admin;
    map["createdAt"] = _createdAt;
    map["groupImg"] = _groupImg;
    map["groupName"] = _groupName;
    map["id"] = _id;
    map["lastMsg"] = _lastMsg;
    map["lastMsgTime"] = _lastMsgTime;
    if (_members != null) {
      map["members"] = _members.map((v) => v).toList();
    }
    map["type"] = _type;
    return map;
  }

}

class GroupRetrieve {
  bool _active;
  String _admin;
  Timestamp _createdAt;
  String _groupImg;
  String _groupName;
  String _id;
  String _lastMsg;
  String _lastMsgTime;
  List<String> _members;
  String _type;

  bool get active => _active;
  String get admin => _admin;
  Timestamp get createdAt => _createdAt;
  String get groupImg => _groupImg;
  String get groupName => _groupName;
  String get id => _id;
  String get lastMsg => _lastMsg;
  String get lastMsgTime => _lastMsgTime;
  List<String> get members => _members;
  String get type => _type;

  GroupRetrieve({
    bool active,
    String admin,
    Timestamp createdAt,
    String groupImg,
    String groupName,
    String id,
    String lastMsg,
    String lastMsgTime,
    List<String> members,
    String type}){
    _active = active;
    _admin = admin;
    _createdAt = createdAt;
    _groupImg = groupImg;
    _groupName = groupName;
    _id = id;
    _lastMsg = lastMsg;
    _lastMsgTime = lastMsgTime;
    _members = members;
    _type = type;
  }

  GroupRetrieve.fromJson(dynamic json) {
    _active = json["active"];
    _admin = json["admin"];
    _createdAt = json["createdAt"];
    _groupImg = json["groupImg"];
    _groupName = json["groupName"];
    _id = json["id"];
    _lastMsg = json["lastMsg"];
    _lastMsgTime = json["lastMsgTime"];
    if (json["members"] != null) {
      _members = [];
      json["members"].forEach((v) {
        _members.add(v);
      });
    }
    _type = json["type"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["active"] = _active;
    map["admin"] = _admin;
    map["createdAt"] = _createdAt;
    map["groupImg"] = _groupImg;
    map["groupName"] = _groupName;
    map["id"] = _id;
    map["lastMsg"] = _lastMsg;
    map["lastMsgTime"] = _lastMsgTime;
    if (_members != null) {
      map["members"] = _members.map((v) => v).toList();
    }
    map["type"] = _type;
    return map;
  }

}