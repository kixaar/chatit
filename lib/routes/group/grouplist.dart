import 'dart:async';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:chatapploydlab/Controllers/firebaseController.dart';
import 'file:///E:/Android/FlutterProjects/ChatExample/chat_app_loyd-master/chat_app_loyd-master/lib/routes/utils/utils.dart';
import 'package:chatapploydlab/Model/response/group.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_widgets/flutter_widgets.dart';
import '../singleChat/chatroom.dart';
import 'group_chat.dart';

class GroupList extends StatefulWidget {
  GroupList(this.myID, this.myName);
  final String myID;
  final String myName;

  @override
  _GroupListState createState() => _GroupListState();
}

class _GroupListState extends State<GroupList> {
  GroupRetrieve groupData;

  List<GroupRetrieve> listOfGroups = [];

  @override
  void initState() {
    getAllGroups();
    FirebaseController.instanace.getUnreadMSGCount();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Chat App - Chat List'),
        centerTitle: true,
      ),
      body: VisibilityDetector(
        key: Key("1"),
        onVisibilityChanged: ((visibility) {
          print('ChatList Visibility code is '+'${visibility.visibleFraction}');
          if (visibility.visibleFraction == 1.0) {
            FirebaseController.instanace.getUnreadMSGCount();
          }
        }),
        child: Padding(
          padding: EdgeInsets.only(top: 20,bottom: 20,left: 30,right: 30),
          child:ListView.builder(
              physics: NeverScrollableScrollPhysics(),
              itemCount: listOfGroups.length,
              shrinkWrap: true,
              itemBuilder: (context,position){
                return itemGroup(position);
              }),
        ),
      ));
  }

  Widget itemGroup(int position) {
    return Padding(
      padding: const EdgeInsets.only(top: 2),
      child: GestureDetector(
        onTap: ()=> {
          Navigator.push(context,      MaterialPageRoute(
              builder: (context) => GroupChat(
                groupData: listOfGroups[position],)
          ))
        },
        child: Container(
          decoration: BoxDecoration(
              color: Colors.black45,
              borderRadius: BorderRadius.circular(40)
          ),
          child: Stack(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 15,top: 10,bottom: 10),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(30),
                      child: CachedNetworkImage(
                        imageUrl: listOfGroups[position].groupImg,
                        placeholder: (context, url) => Container(
                          transform:
                          Matrix4.translationValues(0, 0, 0),
                          child: Container(width: 60,height: 60,
                              child: Center(child:new CircularProgressIndicator())),),
                        errorWidget: (context, url, error) => new Icon(Icons.error),
                        width: 60,height: 60,fit: BoxFit.cover,
                      ),
                    ),

                  ),

                  Padding(
                    padding: EdgeInsets.only(left: 10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(listOfGroups[position].groupName,style: TextStyle(
                            color: Colors.white,
                            fontSize: 16
                        ),
                          textAlign: TextAlign.start,),
                      ],
                    ),
                  ),
                ],
              ),

              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Padding(
                    padding: EdgeInsets.only(right: 10,top: 5),
                    child: SizedBox(
                      height: 50,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Padding(
                              padding: const EdgeInsets.only(top: 0),
                              child: Container(
                                decoration: BoxDecoration(
                                    color: Colors.transparent,
                                    borderRadius: BorderRadius.circular(20)
                                ),
                              )
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _moveTochatRoom(int position,selectedUserToken, selectedUserID,
      selectedUserName, selectedUserThumbnail) async {
    try {
      String chatID = makeChatId(widget.myID, selectedUserID);
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => ChatRoom(
                  listOfGroups[position].id,
                  listOfGroups[position].groupName,
                  selectedUserToken,
                  selectedUserID,
                  chatID,
                  selectedUserName,
                  selectedUserThumbnail)
          )
      );
    } catch (e) {
      print(e.message);
    }
  }

  getAllGroups() async{
    var currentUid = FirebaseAuth.instance.currentUser.uid;
    FirebaseFirestore.instance.collection('users').
    doc(currentUid)
    .collection('groupsIn').orderBy('groupName').get().then((val) =>
    {
      print('Value of Doc length ${val.docs.length}'),
      val.docs.asMap().forEach((index,element) {
        print(element.id);
        FirebaseFirestore.instance.collection('groups')
            .doc(element.id).get().then((value)
        => {
        groupData =GroupRetrieve.fromJson(value),
          index == val.docs.length-1 ?
          setState(() {
                listOfGroups.add(groupData);listOfGroups.sort((a,b){return a.groupName.toLowerCase().compareTo(b.groupName.toLowerCase());});})
              : listOfGroups.add(groupData)
            ,
        print('The id is ${groupData.id} list Length: '
            '${listOfGroups.length}')
        });

      })
    }
    );

  }
}
