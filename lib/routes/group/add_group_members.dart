import 'package:cached_network_image/cached_network_image.dart';
import 'package:chatapploydlab/Model/response/users.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'create_group.dart';


class AddGroupMembers extends StatefulWidget {
  @override
  _AddGroupMembersPageState createState() => _AddGroupMembersPageState();
}

class _AddGroupMembersPageState extends State<AddGroupMembers> {

  var groupNameTextController= TextEditingController();
  int clickedPosition;
  final Set _saved = Set();
  var userName='';
  var userImageUrl='';
  List<UserRetrieve> listOfUsers=[];
  List<UserRetrieve> listOfUsersForGroup=[];



  @override
  void initState() {
    getUsersToAdd();
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Chat App - Add user'),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Center(
          child: Column(
            children: [

              Padding(
                padding: EdgeInsets.only(left: 50,right: 50,top: 30),
                child: Container(
                  width: double.infinity,
                  height: 60,
                  decoration: BoxDecoration(
                      color: Colors.blueAccent,
                    borderRadius: BorderRadius.circular(30)
                  ),
                  child: InkWell(
                    onTap: ()=>{
                    Navigator.push(context,
                    MaterialPageRoute(
                    builder: (context) =>CreateGroup(listOfUsersForGroup))),
                    },
                    borderRadius: BorderRadius.circular(30),
                    child: Center(
                      child: Text(
                        'Done',
                        style: TextStyle(
                          fontSize: 16,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ),
              ),

              Padding(
                padding: EdgeInsets.only(top: 20,bottom: 20,left: 30,right: 30),
                child:ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: listOfUsers.length,
                    shrinkWrap: true,
                    itemBuilder: (context,position){
                      return itemSearchProfile(position);
                    }),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget itemSearchProfile(int position) {
    return Padding(
      padding: const EdgeInsets.only(top: 2),
      child: GestureDetector(
        onTap: ()=> {},
        child: Container(
          decoration: BoxDecoration(
            color: Colors.black45,
            borderRadius: BorderRadius.circular(40)
          ),
          child: Stack(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 15,top: 10,bottom: 10),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(30),
                        child: CachedNetworkImage(
                          imageUrl: listOfUsers[position].userImageUrl,
                          placeholder: (context, url) => Container(
                            transform:
                            Matrix4.translationValues(0, 0, 0),
                            child: Container(width: 60,height: 60,
                                child: Center(child:new CircularProgressIndicator())),),
                          errorWidget: (context, url, error) => new Icon(Icons.error),
                          width: 60,height: 60,fit: BoxFit.cover,
                        ),
                      ),

                  ),

                  Padding(
                    padding: EdgeInsets.only(left: 10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(listOfUsers[position].name,style: TextStyle(
                          color: Colors.white,
                          fontSize: 16
                        ),
                          textAlign: TextAlign.start,),
                      ],
                    ),
                  ),
                ],
              ),

              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Padding(
                    padding: EdgeInsets.only(right: 10,top: 5),
                    child: SizedBox(
                      height: 50,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Padding(
                              padding: const EdgeInsets.only(top: 0),
                              child: Container(
                                decoration: BoxDecoration(
                                    color: Colors.transparent,
                                    borderRadius: BorderRadius.circular(20)
                                ),
                                child: Checkbox(
                            value: _saved.contains(position),
                                 onChanged: (value){
                              setState(() {
                                if(value == true){
                                  _saved.add(position);
                                  listOfUsersForGroup.add(listOfUsers[position]);
                                } else{
                                  _saved.remove(position);
                                  listOfUsersForGroup.remove(listOfUsers[position]);
                                }
                                print(listOfUsersForGroup.length);
                              });
                                 },
                                ),
                              )
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  _showDialog(String msg) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            content: Text(msg),
          );
        });
  }

  getUsersToAdd()async{
    await FirebaseFirestore.instance.collection('users').
    orderBy('createdAt', descending: true).get().then((value) => {
      value.docs.forEach((element) {
        var userData = UserRetrieve.fromJson(element);
        if(element.data()['userId']==FirebaseAuth.instance.currentUser.uid)return;
        else listOfUsers.add(userData);
        print(element.data()['name']);
        print(listOfUsers[0].userImageUrl);
      })
    });
    setState(() {
      listOfUsers.length;
    });
  }
}