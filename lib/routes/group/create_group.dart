import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:chatapploydlab/Model/response/group.dart';
import 'package:chatapploydlab/Model/response/users.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';


class CreateGroup extends StatefulWidget {
  final List<UserRetrieve> listOfSelectedUsers;

  CreateGroup(this.listOfSelectedUsers);

  @override
  _CreateGroupPageState createState() => _CreateGroupPageState();
}

class _CreateGroupPageState extends State<CreateGroup> {
  final imagePicker = ImagePicker();
  var groupNameTextController= TextEditingController();

  List<UserRetrieve> listOfSelectedUser =[];

  File _image;

  List<String> listOfSelectedUserUids =[];



  @override
  void initState() {
    listOfSelectedUser= widget.listOfSelectedUsers;
    listOfSelectedUser.forEach((element) {
      listOfSelectedUserUids.add(element.userId);
    });
    listOfSelectedUserUids.add(FirebaseAuth.instance.currentUser.uid);
    print(listOfSelectedUserUids.length);
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    listOfSelectedUser= widget.listOfSelectedUsers;
    return Scaffold(
      appBar: AppBar(
        title: Text('Chat App - Add user'),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Center(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 50,left: 30,right: 30),
                child: TextField(
                  controller: groupNameTextController,
                  style: TextStyle(
                    color: Colors.black,
                  ),
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.black,
                        width: 2,
                      )
                    ),
                    hintText: 'Group Name',
                  ),
                ),
              ),

              Padding(
                padding: EdgeInsets.only(top: 20),
                child:GestureDetector(
                  onTap: ()=>{
                    getImage()
                  },
                  child: Container(
                    color: Colors.black,
                    height: 150,
                    width: 150,
                    child:_image == null ?Center(
                      child: Text(
                        'Add Image',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 16
                        ),
                      ),
                    ): Image.file(_image,
                    fit: BoxFit.fill,),
                  ),
                )
              ),


              Padding(
                padding: EdgeInsets.only(top: 20,bottom: 20,left: 30,right: 30),
                child:ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: listOfSelectedUser.length,
                    shrinkWrap: true,
                    itemBuilder: (context,position){
                      return itemSearchProfile(position);
                    }),
              ),

              Padding(
                padding: EdgeInsets.only(left: 50,right: 50,top: 30,bottom: 30),
                child: Container(
                  width: double.infinity,
                  height: 60,
                  decoration: BoxDecoration(
                      color: Colors.blueAccent,
                      borderRadius: BorderRadius.circular(30)
                  ),
                  child: InkWell(
                    onTap: ()=>{
                      createGroup()
                    },
                    borderRadius: BorderRadius.circular(30),
                    child: Center(
                      child: Text(
                        'Create Group',
                        style: TextStyle(
                          fontSize: 16,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ),
              ),

            ],
          ),
        ),
      ),
    );
  }

  Future getImage() async {
    final pickedFile = await imagePicker.getImage(source: ImageSource.gallery);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  Widget itemSearchProfile(int position) {
    return Padding(
      padding: const EdgeInsets.only(top: 2),
      child: GestureDetector(
        onTap: ()=>{},
        child: Container(
          decoration: BoxDecoration(
            color: Colors.black45,
            borderRadius: BorderRadius.circular(30)
          ),
          child: Stack(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 15,top: 10,bottom: 10),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(30),
                      child: CachedNetworkImage(
                        imageUrl: listOfSelectedUser[position].userImageUrl,
                        placeholder: (context, url) => Container(
                          transform:
                          Matrix4.translationValues(0, 0, 0),
                          child: Container(width: 60,height: 60,
                              child: Center(child:new CircularProgressIndicator())),),
                        errorWidget: (context, url, error) => new Icon(Icons.error),
                        width: 60,height: 60,fit: BoxFit.cover,
                      ),
                    ),

                  ),

                  Padding(
                    padding: EdgeInsets.only(left: 10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          listOfSelectedUser[position].name,
                          style: TextStyle(
                          color: Colors.white,
                          fontSize: 16
                        ),
                          textAlign: TextAlign.start,),
                      ],
                    ),
                  ),
                ],
              ),

              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Padding(
                    padding: EdgeInsets.only(right: 10,top: 5),
                    child: GestureDetector(
                      onTap: ()=>{
                        setState((){
                          listOfSelectedUser.removeAt(position);
                          listOfSelectedUserUids.removeAt(position);
                          print("${listOfSelectedUser.length} - "
                              "${listOfSelectedUserUids.length}");
                        })
                      },
                      child: SizedBox(
                        height: 40,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Padding(
                                padding: const EdgeInsets.only(top: 5),
                                child: Container(
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(20)
                                  ),
                                  child: Icon(
                                    Icons.close,
                                    size: 15,
                                    color: Colors.black,
                                  ),
                                )
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  _showDialog(String msg) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            content: Text(msg),
          );
        });
  }

  createGroup() async{
    var uniqueDocId = FirebaseFirestore.instance.collection('groups').doc().id;
    var group = GroupSave(
      createdAt: FieldValue.serverTimestamp(),
      active: true,
      admin:FirebaseAuth.instance.currentUser.uid,
      groupName:groupNameTextController.text,
      id: uniqueDocId,
      lastMsg: 'Last message',
      groupImg: await uploadGroupImage(uniqueDocId),
      lastMsgTime:'2020',
      members: listOfSelectedUserUids
    );
    FirebaseFirestore.instance.collection('groups').doc(uniqueDocId)
        .set(group.toJson());
    listOfSelectedUserUids.forEach((element) {
      FirebaseFirestore.instance.collection('users').doc(element)
          .collection('groupsIn').doc(uniqueDocId)
          .set({'groupName':groupNameTextController.text}).whenComplete(() =>
      print('Complete'));
    });

  }

  Future<String> uploadGroupImage(String uniqueDocId) async{
    String filePath = 'groupImage/$uniqueDocId/'
        '${groupNameTextController.text}';
    final StorageReference storageReference = FirebaseStorage().ref().child(filePath);
    final StorageUploadTask uploadTask = storageReference.putFile(_image);
    StorageTaskSnapshot storageTaskSnapshot = await uploadTask.onComplete;
    String imageURL = await storageTaskSnapshot.ref.getDownloadURL();
    return imageURL;
  }
}