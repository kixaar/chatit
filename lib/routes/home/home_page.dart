import 'dart:io';
import 'package:chatapploydlab/Controllers/firebaseController.dart';
import '../group/add_group_members.dart';
import '../singleChat/chatlist.dart';
import 'file:///E:/Android/FlutterProjects/ChatExample/chat_app_loyd-master/chat_app_loyd-master/lib/routes/utils/utils.dart';
import 'package:chatapploydlab/Model/response/users.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:chatapploydlab/Controllers/notificationController.dart';
import 'package:chatapploydlab/Controllers/pickImageController.dart';
import 'package:flutter_widgets/flutter_widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../group/grouplist.dart';

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with WidgetsBindingObserver{
  TextEditingController _nameTextController = TextEditingController();
  TextEditingController _introTextController = TextEditingController();
  File _userImageFile = File('');
  String _userImageUrlFromFB = '';
  bool _isLoading = false;

  @override
  void initState() {
    Firebase.initializeApp();
    NotificationController.instance.takeFCMTokenWhenAppLaunch();
    NotificationController.instance.initLocalNotification();
    setCurrentChatRoomID('none');
    WidgetsBinding.instance.addObserver(this);
    super.initState();
  }
  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }
  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if(state == AppLifecycleState.resumed) {
      }
    super.didChangeAppLifecycleState(state);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Chat App - Add user'),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: VisibilityDetector(
          key: Key("1"),
          onVisibilityChanged: ((visibility) {
            print(visibility.visibleFraction);
            if (visibility.visibleFraction == 1.0) {
              FirebaseController.instanace.getUnreadMSGCount();
            }
          }),
          child: Stack(
            children: <Widget>[
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left: 18.0, top: 10),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        'Your Information.',
                        style:
                        TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  Row(
                    children: <Widget>[
                      Padding(
                          padding: EdgeInsets.all(8),
                          child: GestureDetector(
                              onTap: () {
                                PickImageController.instance.cropImageFromFile()
                                    .then((croppedFile) {
                                  if (croppedFile != null) {
                                    setState(() {
                                      _userImageFile = croppedFile;
                                      _userImageUrlFromFB = '';
                                    });
                                  } else {
                                    _showDialog('Pick Image error');
                                  }
                                });
                              },
                              child: Container(
                                width: 140,
                                height: 160,
                                child: Card(
                                  child: _userImageUrlFromFB != ''
                                      ? Image.network(_userImageUrlFromFB)
                                      : (_userImageFile.path != '')
                                      ? Image.file(_userImageFile,
                                      fit: BoxFit.fill)
                                      : Icon(Icons.add_photo_alternate,
                                      size: 60, color: Colors.grey[700]),
                                ),
                              ))),
                      Expanded(
                        child: Column(
                          children: <Widget>[
                            TextFormField(
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  icon: Icon(Icons.account_circle),
                                  labelText: 'Name',
                                  hintText: 'Type Name'),
                              controller: _nameTextController,
                            ),
                            TextFormField(
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  icon: Icon(Icons.note),
                                  labelText: 'Password',
                                  hintText: 'Type Password'),
                              controller: _introTextController,
                            ),
                          ],
                        ),
                      )
                    ],
                  ),

                  Padding(
                      padding: const EdgeInsets.fromLTRB(20.0, 10, 20, 10),
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              'Go to Chat list',
                              style: TextStyle(fontSize: 28),
                            )
                          ],
                        ),
                        textColor: Colors.white,
                        color: Colors.green[700],
                        padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                        onPressed: () {
                          Navigator.push(context,
                              MaterialPageRoute(
                                  builder: (context) => ChatList(
                                      FirebaseAuth.instance.currentUser.uid,
                                      _nameTextController.text)));
                        },
                      )),

                  Padding(
                      padding: const EdgeInsets.fromLTRB(20.0, 10, 20, 10),
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              'Register User',
                              style: TextStyle(fontSize: 28),
                            )
                          ],
                        ),
                        textColor: Colors.white,
                        color: Colors.green[700],
                        padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                        onPressed: () {
                          registerUserWithEP();
                        },
                      )),

                  Padding(
                      padding: const EdgeInsets.fromLTRB(20.0, 10, 20, 10),
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              'Login User',
                              style: TextStyle(fontSize: 28),
                            )
                          ],
                        ),
                        textColor: Colors.white,
                        color: Colors.green[700],
                        padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                        onPressed: () {
                          loginUserWithEP();
                        },
                      )),

                  Padding(
                      padding: const EdgeInsets.fromLTRB(20.0, 10, 20, 10),
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              'Create Group',
                              style: TextStyle(fontSize: 28),
                            )
                          ],
                        ),
                        textColor: Colors.white,
                        color: Colors.green[700],
                        padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                        onPressed: () {
                          Navigator.push(context,
                              MaterialPageRoute(
                                  builder: (context) =>AddGroupMembers()));
                        },
                      )),

                  Padding(
                      padding: const EdgeInsets.fromLTRB(20.0, 10, 20, 10),
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              'Go To Group Chat',
                              style: TextStyle(fontSize: 28),
                            )
                          ],
                        ),
                        textColor: Colors.white,
                        color: Colors.green[700],
                        padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                        onPressed: () {
                          Navigator.push(context,
                              MaterialPageRoute(
                                  builder: (context) =>GroupList(
                                      FirebaseAuth.instance.currentUser.uid,
                                      FirebaseAuth.instance.currentUser.uid)));
                        },
                      )),
                ],
              ),
              Positioned(
                // Loading view in the center.
                child: _isLoading
                    ? Container(
                  child: Center(
                    child: CircularProgressIndicator(),
                  ),
                  color: Colors.white.withOpacity(0.7),
                )
                    : Container(),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _saveDataToServer() {
    setState(() {
      _isLoading = true;
    });
    String alertString = checkValidUserData(_userImageFile, _userImageUrlFromFB,
        _nameTextController.text, _introTextController.text);
    if (alertString.trim() != '') {
      _showDialog(alertString);
    } else {
      _userImageUrlFromFB != ''
          ? FirebaseController.instanace.saveUserDataToFirebaseDatabase(randomIdWithName(_nameTextController.text),
          _nameTextController.text,_introTextController.text,_userImageUrlFromFB).then((data){
        _moveToChatList(data);
      })
          : FirebaseController.instanace.saveUserImageToFirebaseStorage(
          randomIdWithName(_nameTextController.text),_nameTextController.text,_introTextController.text,
          _userImageFile).then((data){
        _moveToChatList(data);
      });
    }
  }

  _moveToChatList(data) {
    setState(() { _isLoading = false; });
    if(data != null) {

    }
    else { _showDialog('Save user data error'); }
  }

  _showDialog(String msg) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            content: Text(msg),
          );
        });
  }

  void registerUserWithEP(){
    FirebaseAuth.instance.
    createUserWithEmailAndPassword(email: _nameTextController.text,
        password: _introTextController.text)
        .then((value) async {
      String filePath = 'userProfileImages/${value.user.uid}';
      final StorageReference storageReference = FirebaseStorage().ref().child(filePath);
      final StorageUploadTask uploadTask = storageReference.putFile(_userImageFile);
      StorageTaskSnapshot storageTaskSnapshot = await uploadTask.onComplete;

      String imageURL = await storageTaskSnapshot.ref.getDownloadURL();
       SharedPreferences prefs = await SharedPreferences.getInstance();
        var user =UserSave(
            fCMToken: prefs.get('FCMToken'),
          name: _nameTextController.text,
          userId: value.user.uid,
          createdAt:FieldValue.serverTimestamp(),
          userImageUrl:imageURL,
          intro: 'Hi,I am New'
        );
          FirebaseFirestore.instance
        .collection('users')
        .doc(value.user.uid)
        .set(user.toJson());
    })
        .catchError((onError)=>{
          print(onError.toString())
    });

  }

  void loginUserWithEP()async{
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setString('FCMToken',null);
    FirebaseAuth.instance.
    signInWithEmailAndPassword(email: _nameTextController.text,
        password: _introTextController.text)
        .then((value) => {
          print(value.user.uid),
          updateToken()
        })
        .catchError((onError)=>{
      print(onError.toString())
    });
  
  }

  updateToken()async{
    SharedPreferences pref = await SharedPreferences.getInstance();
    await NotificationController.instance.takeFCMTokenWhenAppLaunch();
    print("FCM Token Is: ${pref.get('FCMToken')}");
    FirebaseFirestore.instance.collection('users')
        .doc(FirebaseAuth.instance.currentUser.uid)
        .update({'FCMToken':pref.get('FCMToken')}).then((value) => {
    print('FCMToken Update')
    });
    print("FCM Token Is: ${pref.get('FCMToken')}");

  }

  _takeUserInformationFromFBDB() async {
    FirebaseController.instanace.takeUserInformationFromFBDB().then((documents) {
      if (documents.length > 0) {
        _nameTextController.text = documents[0]['name'];
        _introTextController.text = documents[0]['intro'];
        setState(() {
          _userImageUrlFromFB = documents[0]['userImageUrl'];
        });
      }
      setState(() {
        _isLoading = false;
      });
    });
  }

}