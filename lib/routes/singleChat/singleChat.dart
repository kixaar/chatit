import 'package:cached_network_image/cached_network_image.dart';
import 'package:chatapploydlab/Controllers/firebaseController.dart';
import 'package:chatapploydlab/Controllers/notificationController.dart';
import 'package:chatapploydlab/Controllers/pickImageController.dart';
import 'file:///E:/Android/FlutterProjects/ChatExample/chat_app_loyd-master/chat_app_loyd-master/lib/routes/utils/utils.dart';
import 'package:chatapploydlab/Model/response/message.dart';
import 'package:chatapploydlab/routes/singleChat/single_chat_settings.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../utils/fullphoto.dart';

class SingleChat extends StatefulWidget{
   final String chatWithUserId;

  SingleChat({this.chatWithUserId});

  @override
  _SingleChatScreenState createState() => _SingleChatScreenState();
}


class _SingleChatScreenState extends State<SingleChat> {
  var radioValue;
  double halfScreenSize;
  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey();
  TextEditingController msgTextController = TextEditingController();
  List<MessageReceive> listOfMessages=[];
  ScrollController chatListScrollController = ScrollController();
  String messageType = 'Text';
  bool _isLoading = false;
  var messageOptionsVisibility = false;
  Color backgroundColorOnMsgOptions = Color(0xFF4FC3F7);

  var tappedIndexOfItem;

  Map<String,dynamic> selectedMsgItem;

  @override
  void dispose() {
    setCurrentChatRoomID('none');
    super.dispose();
  }


  @override
  void initState() {
    setCurrentChatRoomID(widget.chatWithUserId);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    halfScreenSize=MediaQuery.of(context).size.width/1.5;
    return Scaffold(
      key: scaffoldKey,
        backgroundColor: Colors.black,
        body: StreamBuilder<DocumentSnapshot>(
          stream:FirebaseFirestore.instance.collection('users')
          .doc(widget.chatWithUserId)
              .snapshots(),
          builder: (context,chatToUserSnapshot){
            return Stack(
              children: [
                //ListView For Chat
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [

                    Expanded(
                      child: Padding(
                        padding: EdgeInsets.only(top: 140,bottom: 50),
                        child:StreamBuilder<QuerySnapshot>(
                          stream: FirebaseFirestore.instance
                              .collection('singleChat')
                              .doc('${FirebaseAuth.instance.currentUser.uid}-'
                              '${widget.chatWithUserId}')
                              .collection('messages')
                              .orderBy('datetime', descending: true)
                              .snapshots(),
                          builder: (context,snapshot)
                          {
                            if(snapshot.hasData){
                              for(var data in snapshot.data.docs){
                                if(data.get('receiver')==
                                    FirebaseAuth.instance.currentUser.uid
                                    &&data.get('msgseen')== false)
                                  {
                                    var msgForOther = data.get('messageId');
                                    if(data.reference!=null)
                                      {
                                        FirebaseFirestore.instance
                                            .runTransaction((transaction)async{
                                           transaction.update(data.reference,
                                               {'msgseen':true});

                                           FirebaseFirestore.instance
                                               .collection('singleChat')
                                               .doc('${widget.chatWithUserId}-'
                                               '${FirebaseAuth.instance.currentUser.uid}')
                                               .collection('messages').doc(msgForOther).update(
                                             {'msgseen':true});

                                        }
                                        );
                                      }
                                  }
                              }
                            };
                            return Container(
                              child: ListView.builder(
                                  reverse: true,
                                  physics: AlwaysScrollableScrollPhysics(),
                                  itemCount: snapshot.hasData?
                                  snapshot.data.docs.length
                                      :0,
                                  shrinkWrap: true,
                                  controller: chatListScrollController,
                                  itemBuilder: (context,position){
                                    return snapshot.hasData
                                        ?setItemByUser(position,snapshot.data)
                                        :Center(child: LinearProgressIndicator());
                                  }),
                            );
                          },
                        ),
                      ),
                    ),

                  ],
                ),
                //Top Group Icon Name
                Stack(
                  children: [
                    Container(
                      height: 145,
                      decoration: BoxDecoration(
                          color: Colors.blueGrey,
                          border: Border(
                              bottom: BorderSide(
                                  color: Colors.white,
                                  width: 1)
                          )
                      ),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 40,left: 20),
                            child: GestureDetector(
                              onTap: () => {},
                              child: Container(
                                color: Colors.blueGrey,

                                child: Stack(
                                  children: [
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        IconButton(
                                          onPressed: ()=>{
                                            Navigator.push(context,
                                            MaterialPageRoute(
                                            builder: (context) =>SingleChatSettings(
                                              widget.chatWithUserId
                                            ))),
                                          },
                                          icon: Icon(
                                            Icons.more_horiz,
                                            color: Colors.white,
                                          ),
                                        )
                                      ],
                                    ),
                                    chatToUserSnapshot.hasData?Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.only(
                                              left: 10, top: 10, bottom: 10),
                                          child: ClipRRect(
                                            borderRadius: BorderRadius.circular(30),
                                            child: CachedNetworkImage(
                                              imageUrl: chatToUserSnapshot.data.get('userImageUrl'),
                                              placeholder: (context, url) => Container(
                                                transform:
                                                Matrix4.translationValues(0, 0, 0),
                                                child: Container(
                                                    width: 60,
                                                    height: 60,
                                                    child: Center(
                                                        child:
                                                        new CircularProgressIndicator())),
                                              ),
                                              errorWidget: (context, url, error) =>
                                              new Icon(Icons.error),
                                              width: 60,
                                              height: 60,
                                              fit: BoxFit.cover,
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(left: 20),
                                          child: Column(
                                            mainAxisAlignment: MainAxisAlignment.start,
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            children: [
                                              Text(chatToUserSnapshot.data.get('name'),
                                                style: TextStyle(
                                                    fontSize: 16,
                                                    color: Colors.white
                                                ),
                                                textAlign: TextAlign.start,),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ):
                                    Container(),

                                  ],
                                ),
                              ),
                            ),
                          ),

                        ],
                      ),
                    ),

                    Visibility(
                      visible: messageOptionsVisibility,
                      maintainAnimation: true,
                      maintainState: true,
                      child: Container(
                        height: 145,
                        padding: EdgeInsets.only(left: 20,right: 20),
                        decoration: BoxDecoration(
                            color: Colors.blueGrey,
                            border: Border(
                                bottom: BorderSide(
                                    color: Colors.white,
                                    width: 1)
                            )
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            IconButton(
                              padding: EdgeInsets.all(2),
                              icon: Icon(
                                Icons.arrow_back,
                                color: Colors.white,
                                size: 30,
                              ),
                              onPressed: ()=>{
                                setState((){
                                  messageOptionsVisibility = false;
                                  backgroundColorOnMsgOptions = Colors.transparent;
                                })
                              },
                            ),
                            IconButton(
                              padding: EdgeInsets.all(2),
                              icon: Icon(
                                Icons.star,
                                color: Colors.white,
                                size: 30,
                              ),
                              onPressed: ()=>{
                                FirebaseFirestore.instance.collection('users')
                                    .doc(FirebaseAuth.instance.currentUser.uid)
                                    .collection('starredMessages')
                                    .doc(selectedMsgItem['messageId'])
                                    .set(selectedMsgItem).whenComplete(() => {
                                  print('Message Starred'),
                                  setState(() {
                                    messageOptionsVisibility = false;
                                    backgroundColorOnMsgOptions =
                                        Colors.transparent;
                                  })
                                })
                              },
                            ),
                            IconButton(
                              padding: EdgeInsets.all(2),
                              icon: Icon(
                                Icons.delete,
                                color: Colors.red,
                                size: 30,
                              ),
                              onPressed: ()=>{
                                setState((){
                                  messageOptionsVisibility = false;
                                  backgroundColorOnMsgOptions = Colors.transparent;
                                }),
                                ///Delete Group Message Code
                                FirebaseFirestore.instance.collection('singleChat')
                                .doc("${FirebaseAuth.instance.currentUser.uid}-"
                                    "${widget.chatWithUserId}")
                                .collection('messages')
                                .doc(selectedMsgItem['messageId'])
                                .delete().whenComplete(() => print('Message Deleted')),
                                FirebaseFirestore.instance
                                    .collection('users')
                                    .doc(FirebaseAuth.instance.currentUser.uid)
                                    .collection('starredMessages')
                                    .doc(selectedMsgItem['messageId'])
                                   .delete().whenComplete(() => print('Deleted')),
                              },
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
                //Text Message Field
                Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Container(
                      margin:EdgeInsets.only(left: 5,right: 5),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        color: Colors.white,
                      ),
                      child: Stack(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Expanded(
                                child: TextField(
                                  controller: msgTextController,
                                  minLines: 1,
                                  maxLines: 5,
                                  decoration: InputDecoration(
                                      contentPadding: EdgeInsets.only(left: 15,right: 90,bottom: 10,top: 10)
                                  ),
                                ),
                              ),
                            ],
                          ),
                          StreamBuilder<DocumentSnapshot>(
                            stream: FirebaseFirestore.instance.collection('users')
                                .doc(widget.chatWithUserId)
                                .collection('blockList')
                                .doc(FirebaseAuth.instance.currentUser.uid)
                                .snapshots(),
                            builder: (context,chatWithUserSnapshot){
                              return  Container(
                                child: StreamBuilder<DocumentSnapshot>(
                                  stream: FirebaseFirestore.instance.collection('users')
                                      .doc(FirebaseAuth.instance.currentUser.uid)
                                      .collection('blockList')
                                      .doc(widget.chatWithUserId)
                                      .snapshots(),
                                  builder: (context,currentUserSnapshot){
                                    return Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        IconButton(
                                          onPressed: (){
                                            print(currentUserSnapshot.data.exists);
                                            print(chatWithUserSnapshot.data.exists);
                                            currentUserSnapshot.data.exists == false&&
                                            chatWithUserSnapshot.data.exists == false
                                                ?PickImageController.instance.cropImageFromFile()
                                                .then((croppedFile) {
                                              if (croppedFile != null) {
                                                setState(() {
                                                  messageType = 'Image'; _isLoading = true; });
                                                _saveUserImageToFirebaseStorage(chatToUserSnapshot.data,croppedFile);
                                              }else {
                                                // _showDialog('No Image Selected');
                                              }
                                            }):chatWithUserSnapshot.data.exists
                                                ?_showDialog('User Has Blocked You')
                                            :_showDialog('You have Blocked The User');
                                          },
                                          padding: EdgeInsets.zero,
                                          alignment: Alignment.centerRight,
                                          icon: Icon(
                                            Icons.camera_alt,
                                            color: Colors.black,
                                            size: 20,
                                          ),
                                        ),
                                        IconButton(
                                          onPressed: ()=>{
                                            currentUserSnapshot.data.exists == false &&
                                            chatWithUserSnapshot.data.exists== false
                                                ?msgTextController.text.isNotEmpty?
                                                sendMessageSingleChat(chatToUserSnapshot.data)
                                                :{}
                                                :chatWithUserSnapshot.data.exists
                                                ?_showDialog('User Has Blocked You')
                                                :_showDialog('You have Blocked The User'),
                                          },
                                          padding: EdgeInsets.zero,
                                          alignment: Alignment.center,
                                          iconSize: 20,
                                          icon: Icon(
                                            Icons.send,
                                            color: Colors.black,
                                          ),
                                        )
                                      ],
                                    );
                                  },
                                ),
                              );
                            },
                          ),
                        ],
                      ),
                    ),

                  ],
                )
              ],
            );
          },
        )
    );
  }

  Widget itemLeft(int position,QuerySnapshot snapshot){
    return snapshot.docs[position].get('type')=='Text'
        ?GestureDetector(
      onLongPress: ()=>{
        setState((){
          backgroundColorOnMsgOptions = Color(0xFF4FC3F7);
          messageOptionsVisibility = true;
          tappedIndexOfItem = position;
          selectedMsgItem = snapshot.docs[position].data();
        })
      },
          child: Container(
            color: tappedIndexOfItem== position
                ?backgroundColorOnMsgOptions:Colors.transparent,
            child: Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
               Flexible(
                 flex: 5,
                 child: Wrap(
                   children: [
                     Container(
                       decoration: BoxDecoration(
                           color:Colors.blueGrey,
                           borderRadius: BorderRadius.circular(8)),
                       margin: EdgeInsets.only(left: 10,top: 7,bottom: 7),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                        Wrap(
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(5),
                              child: Text(
                                    snapshot.docs[position].get('senderName'),
                                style: TextStyle(
                                    fontSize: 16,
                                    color: Colors.orangeAccent
                                ),
                                  ),
                            ),
                          ],
                        ),
                          Wrap(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(left: 15,right: 15,bottom: 10),
                                child: Text(
                                      snapshot.docs[position].get('message'),
                                  style: TextStyle(
                                      fontSize: 16,
                                      color: Colors.white
                                  ),

                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
              ),
                   ],
                 ),
               ),
            Flexible(
              flex: 2,
              child: Padding(
                padding: const EdgeInsets.only(left: 5,bottom: 7),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(
                      snapshot.docs[position].get('datetime')!= null?
                      DateFormat.jm().format(snapshot.docs[position].get('datetime')
                          .toDate()) :'fetching',
                      style: TextStyle(
                          fontSize: 16,
                          color: Colors.white
                      ),
                    ),
                  ],
                ),
              ),
            ),

            Spacer(
              flex: 1,
            )
      ],
    ),
          ),
        )
        :GestureDetector(
      onLongPress: ()=>{
        setState((){
          backgroundColorOnMsgOptions = Color(0xFF4FC3F7);
          messageOptionsVisibility = true;
          tappedIndexOfItem = position;
          selectedMsgItem = snapshot.docs[position].data();

        })
      },
          child: Container(
            color: tappedIndexOfItem== position
                ?backgroundColorOnMsgOptions:Colors.transparent,
            child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                     Column(
                       children: [
                         Container(
                           margin: EdgeInsets.only(top: 10,left: 10),
                           width: halfScreenSize,
                           decoration: BoxDecoration(
                               color: Colors.blueGrey,
                               borderRadius: BorderRadius.only(
                                 topLeft: Radius.circular(5),
                                 topRight: Radius.circular(5),
                               )
                           ),
                           child: Padding(
                             padding: const EdgeInsets.only(left: 5,top: 5,bottom: 5),
                             child: Text(
                               snapshot.docs[position].get('senderName'),
                               style: TextStyle(
                                   color: Colors.orangeAccent,
                                   fontSize: 16
                               ),
                             ),
                           ),
                         ),
                         Container(
                              margin: EdgeInsets.only(left: 10,bottom: 10),
                              width:halfScreenSize,
                              height: halfScreenSize,
                              child:  GestureDetector(
                              onTap: ()=>{
                               Navigator.push(
                               context, MaterialPageRoute(builder: (context) =>
                                FullPhoto(url: snapshot.docs[position].get('message'))))
                                  },
                                child: ClipRRect(
                                  borderRadius:BorderRadius.only(
                                      bottomLeft: Radius.circular(5),
                                      bottomRight: Radius.circular(5)
                                  ),
                                  child: CachedNetworkImage(
                                    imageUrl: snapshot.docs[position].get('message'),
                                    placeholder: (context, url) => Container(
                                      transform:
                                      Matrix4.translationValues(0, 0, 0),
                                      child: Container(
                                          width: halfScreenSize,
                                          height: halfScreenSize,
                                          child: Center(
                                              child:
                                              new CircularProgressIndicator())),
                                    ),
                                    errorWidget: (context, url, error) =>
                                    new Icon(Icons.error),
                                    width: halfScreenSize,
                                    height: halfScreenSize,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                            ),
                       ],
                     ),

                    Flexible(
                      flex: 2,
                      child: Padding(
                        padding: const EdgeInsets.only(left: 5,bottom: 7),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Text(
                              snapshot.docs[position].get('datetime')!= null?
                              DateFormat.jm().format(snapshot.docs[position].get('datetime')
                                  .toDate()) :'fetching',
                              style: TextStyle(
                                  fontSize: 16,
                                  color: Colors.white
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),

                    Spacer(
                      flex: 1,
                    )

                  ],
                ),
          ),
        );
  }

  Widget itemRight(int position,QuerySnapshot snapshot){
    print("Message read Status ${snapshot.docs[position].get('msgseen')}");
    return snapshot.docs[position].get('type')=='Text'
        ?GestureDetector(
        onLongPress: ()=>{
          setState((){
            backgroundColorOnMsgOptions = Color(0xFF4FC3F7);
            messageOptionsVisibility = true;
            tappedIndexOfItem = position;
            selectedMsgItem = snapshot.docs[position].data();
          })
        },
          child: Container(
            color: tappedIndexOfItem== position
                ?backgroundColorOnMsgOptions:Colors.transparent,
            child: Row(
      mainAxisAlignment: MainAxisAlignment.end,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
            Spacer(
              flex: 1,
            ),

            Flexible(
              flex: 2,
              child: Padding(
                padding: const EdgeInsets.only(right: 5,bottom: 7),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(
                      snapshot!=null?
                      snapshot.docs[position].get('datetime')!= null?
                      DateFormat.jm().format(snapshot.docs[position].get('datetime')
                          .toDate()) :'sending':'fetching',
                      style:TextStyle(
                          fontSize: 16,
                          color: Colors.white
                      ),
                    ),
                  ],
                ),
              ),
            ),

            Flexible(
              flex: 5,
              child: Wrap(
                children: [
                  Container(
                    decoration: BoxDecoration(
                        color:Colors.blueGrey,
                        borderRadius: BorderRadius.circular(8)),
                    margin: EdgeInsets.only(right: 10,top: 7,bottom: 7),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [

                        Wrap(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 15,right: 15,bottom: 0,top: 10),
                              child: Text(
                                snapshot.docs[position].get('message'),
                                style: TextStyle(
                                    fontSize: 16,
                                    color: Colors.white
                                ),
                              ),
                            ),
                          ],
                        ),


                            Padding(
                          padding: const EdgeInsets.only(right: 5,bottom: 2),
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment:MainAxisAlignment.end,
                            children: [
                              snapshot.docs[position].get('msgseen')== false
                                  ? Visibility(
                                    visible: true,
                                    child: Icon(
                                      Icons.done,
                                      color: Colors.white,
                                      size: 10,
                                    ),
                                  )
                                  : Visibility(
                                    visible: true,
                                    child: Icon(
                                      Icons.done_all,
                                      color: Colors.orangeAccent,
                                      size: 10,
                                    ),
                                  ),


                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
      ],
    ),
          ),
        )
        :GestureDetector(
      onLongPress: ()=>{
        setState((){
          backgroundColorOnMsgOptions = Color(0xFF4FC3F7);
          messageOptionsVisibility = true;
          tappedIndexOfItem = position;
          selectedMsgItem = snapshot.docs[position].data();
        })
      },
          child: Container(
            color: tappedIndexOfItem== position
                ?backgroundColorOnMsgOptions:Colors.transparent,
            child: Row(
      mainAxisAlignment: MainAxisAlignment.end,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
                  Spacer(
                    flex: 1,
                  ),
                  Flexible(
                    flex: 2,
                    child: Padding(
                      padding: const EdgeInsets.only(right: 5, bottom: 7),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Text(
                            snapshot != null
                                ? snapshot.docs[position].get('datetime') !=
                                        null
                                    ? DateFormat.jm().format(snapshot
                                        .docs[position]
                                        .get('datetime')
                                        .toDate())
                                    : 'sending'
                                : 'fetching',
                            style: TextStyle(fontSize: 16, color: Colors.white),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Stack(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 10, top: 10, bottom: 10),
                        width: halfScreenSize,
                        height: halfScreenSize,
                        child: GestureDetector(
                          onTap: () => {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => FullPhoto(
                                        url: snapshot.docs[position]
                                            .get('message')))),
                          },
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(5),
                            child: CachedNetworkImage(
                              imageUrl: snapshot.docs[position].get('message'),
                              placeholder: (context, url) => Container(
                                transform: Matrix4.translationValues(0, 0, 0),
                                child: Container(
                                    width: halfScreenSize,
                                    height: halfScreenSize,
                                    child: Center(
                                        child: new CircularProgressIndicator())),
                              ),
                              errorWidget: (context, url, error) =>
                              new Icon(Icons.error),
                              width: halfScreenSize,
                              height: halfScreenSize,
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(right: 10, top: 10, bottom: 10),
                        width: halfScreenSize,
                        height: halfScreenSize,
                        alignment: Alignment.bottomRight,
                        child:  snapshot.docs[position].get('msgseen')== false
                            ? Visibility(
                          visible: true,
                          child: Icon(
                            Icons.done,
                            color: Colors.white,
                            size: 20,
                          ),
                        )
                            : Visibility(
                          visible: true,
                          child: Icon(
                            Icons.done_all,
                            color: Colors.orangeAccent,
                            size: 20,
                          ),
                        ),
                      )
                    ] ,
                  ),
                ],
    ),
          ),
        );

  }

  Widget setItemByUser(int position,QuerySnapshot snapshot) {
    if(snapshot.docs[position].get('senderID') ==
        FirebaseAuth.instance.currentUser.uid)
      {
        return itemRight(position,snapshot);
      }
    else
      {
        return itemLeft(position,snapshot);
      }
  }

  sendMessageSingleChat(DocumentSnapshot snapshot,[String imageUrl]) async {
    var messageText = msgTextController.text;
    msgTextController.clear();
    var message;
    var uniqueDocId = FirebaseFirestore.instance
        .collection('singleChat')
        .doc('${FirebaseAuth.instance.currentUser.uid}-'
        '${widget.chatWithUserId}')
        .collection('messages').doc().id;

    if(messageType == 'Image')
      {
        messageText = "(Image)";
         message= MessageSend(
            datetime: FieldValue.serverTimestamp(),
            messageId: uniqueDocId,
            message: imageUrl,
            msgseen: false,
            receiver: widget.chatWithUserId,
            senderID: FirebaseAuth.instance.currentUser.uid,
            senderName: FirebaseAuth.instance.currentUser.email,
            type: messageType
        );
      }
    else
      {
        messageType = 'Text';
        message = MessageSend(
            datetime: FieldValue.serverTimestamp(),
            messageId: uniqueDocId,
            message: messageText,
            messageStarred:false,
            msgseen: false,
            receiver: widget.chatWithUserId,
            senderID: FirebaseAuth.instance.currentUser.uid,
            senderName: FirebaseAuth.instance.currentUser.email,
            type: messageType
        );

      }

    await FirebaseFirestore.instance.collection('singleChat')
        .doc("${FirebaseAuth.instance.currentUser.uid}-${widget.chatWithUserId}")
        .collection('messages')
        .doc(uniqueDocId).set(message.toJson()).whenComplete(() =>{

      FirebaseFirestore.instance.collection('singleChat')
          .doc('${widget.chatWithUserId}-${FirebaseAuth.instance.currentUser.uid}')
          .collection('messages')
          .doc(uniqueDocId).set(message.toJson())
          .whenComplete((){
        print('Message Sent');
        sendNotificationMessageToPeerUser(snapshot,messageText: messageText);
      }),
      messageType = 'Text',
    chatListScrollController.jumpTo(0.0),

    });
  }

  Future sendNotificationMessageToPeerUser(DocumentSnapshot value,
      {String messageText}) async{
     await NotificationController.instance.
    sendNotificationMessageToPeerUser(
         await FirebaseController.instanace
             .getUnreadMSGCountForOtherUser(value.get('userId')),
         'text',
    messageText,
        value.get('name'),
         widget.chatWithUserId,
    value.get('FCMToken'));
  }

  _showDialog(String msg){
    showDialog(

        context: context,
        builder:(context) {
          return AlertDialog(

            content: Text(msg),
          );
        }
    );
  }

  Future<void> _saveUserImageToFirebaseStorage(DocumentSnapshot snapshot,croppedFile) async {
    try {
      FirebaseStorage.instance.ref()
          .child('singleChatImages/${FirebaseAuth.instance.currentUser.uid}/'
          '${widget.chatWithUserId}').putFile(croppedFile)
          .onComplete.then((value) =>
      {
        value.ref.getDownloadURL().then((value) => {

          sendMessageSingleChat(snapshot,value.toString())
        })

      });
    }catch(e) {
      _showDialog('Error add user image to storage');
    }
  }

}