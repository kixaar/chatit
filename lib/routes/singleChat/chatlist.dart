import 'package:cached_network_image/cached_network_image.dart';
import 'package:chatapploydlab/Controllers/firebaseController.dart';
import 'file:///E:/Android/FlutterProjects/ChatExample/chat_app_loyd-master/chat_app_loyd-master/lib/routes/utils/utils.dart';
import 'file:///E:/Android/FlutterProjects/ChatExample/chat_app_loyd-master/chat_app_loyd-master/lib/routes/singleChat/singleChat.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_widgets/flutter_widgets.dart';
import 'chatroom.dart';

class ChatList extends StatefulWidget {
  ChatList(this.myID, this.myName);

  final String myID;
  final String myName;

  @override
  _ChatListState createState() => _ChatListState();
}

class _ChatListState extends State<ChatList> {
  var lastMsgValue='';

  @override
  void initState() {
    FirebaseController.instanace.getUnreadMSGCount();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text('Chat App - Chat List'),
        centerTitle: true,
      ),
      body: VisibilityDetector(
        key: Key("1"),
        onVisibilityChanged: ((visibility) {
          print('ChatList Visibility code is '+'${visibility.visibleFraction}');
          if (visibility.visibleFraction == 1.0) {
            FirebaseController.instanace.getUnreadMSGCount();
          }
        }),
        child: StreamBuilder<QuerySnapshot>(
            stream: Firestore.instance.collection('users').
            orderBy('createdAt', descending: true)
                .snapshots(),
            builder: (context, snapshot) {
              if (!snapshot.hasData)
                return Container(
                  child: Center(
                    child: CircularProgressIndicator(),
                  ),
                  color: Colors.white.withOpacity(0.7),
                );
              return countChatListUsers(widget.myID, snapshot) > 0
              ? ListView.builder(
                shrinkWrap: true,
                  itemCount: snapshot.hasData?snapshot.data.docs.length:0,
                  itemBuilder:(context,position){
                    return snapshot.data.docs[position].get('userId')!=
                    FirebaseAuth.instance.currentUser.uid
                    ?itemProfile(snapshot.data,position)
                        :Container();
                  } )
              : Container(
                  child: Center(
                      child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(Icons.forum, color: Colors.grey[700],size: 64,),
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Text(
                          'There are no users except you.\nPlease use '
                              'other devices to chat.',
                          style: TextStyle(fontSize: 18, color: Colors.grey[700]),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ],
                  )),
                );
            }),
      ));
  }

  Future<void> _moveTochatRoom(selectedUserToken, selectedUserID,
      selectedUserName, selectedUserThumbnail) async {
    try {
      String chatID = makeChatId(widget.myID, selectedUserID);
      print(selectedUserID);
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => SingleChat(chatWithUserId: selectedUserID,)));
      // ChatRoom(
      //     widget.myID,
      //     widget.myName,
      //     selectedUserToken,
      //     selectedUserID,
      //     chatID,
      //     selectedUserName,
      //     selectedUserThumbnail)
    } catch (e) {
      print(e.message);
    }
  }

  Widget itemProfile(QuerySnapshot snapshot,int position) {
    return GestureDetector(
      onTap: ()=>{
        _moveTochatRoom(snapshot.docs[position]['FCMToken'],
            snapshot.docs[position]['userId'],
            snapshot.docs[position]['name'],
            snapshot.docs[position]['userImageUrl'])
      },
      child: Padding(
        padding: const EdgeInsets.only(top: 2,left: 5,right: 5),
        child: Container(
          color: Colors.blueGrey,
          child: Stack(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 10,top: 10,bottom: 10),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(15),
                        child: CachedNetworkImage(
                          imageUrl: snapshot.docs[position].get('userImageUrl'),
                          placeholder: (context, url) => Container(
                            transform:
                            Matrix4.translationValues(0, 0, 0),
                            child: Container(width: 40,height: 40,
                                child: Center(child:new CircularProgressIndicator())),),
                          errorWidget: (context, url, error) => new Icon(Icons.error),
                          width: 40,height: 40,fit: BoxFit.cover,
                        ),
                      ),
                  ),

                  Padding(
                    padding: EdgeInsets.only(left: 10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(snapshot.docs[position].get('name'),
                          style: TextStyle(
                          color: Colors.white,
                          fontSize: 11
                        ),
                          textAlign: TextAlign.start,),

                        StreamBuilder<QuerySnapshot>(
                          stream: FirebaseFirestore.instance
                              .collection('singleChat')
                          .doc("${FirebaseAuth.instance.currentUser.uid}-"
                              "${snapshot.docs[position].get('userId')}")
                          .collection('messages')
                          .orderBy('datetime',descending: true)
                          .snapshots(),
                          builder: (context,userLatestMsgSnap){
                            return SizedBox(
                              width: MediaQuery.of(context).size.width/1.4,
                              child: Padding(
                                padding: const EdgeInsets.only(top: 10),
                                child: Text(userLatestMsgSnap.hasData&&
                                    userLatestMsgSnap.data!=null
                                    ? userLatestMsgSnap.data.docs.isNotEmpty
                                    ?userLatestMsgSnap.data.docs[0].get('type')=="Text"
                                    ?userLatestMsgSnap.data.docs[0].get('message')
                                     :'(Image)'
                                     :'Start Conversation'
                                    : 'Start Conversation',
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 11,
                                ),
                                ),
                              ),
                            );
                          },
                        )
                      ],
                    ),
                  ),
                ],
              ),

              StreamBuilder<QuerySnapshot>(
                stream: FirebaseFirestore.instance.collection('singleChat')
                .doc("${FirebaseAuth.instance.currentUser.uid}-"
                    "${snapshot.docs[position].get('userId')}")
                .collection('messages')
                .where('msgseen',isEqualTo: false)
                .where('senderID',isEqualTo:snapshot.docs[position].get('userId'))
                .snapshots(),
                builder: (context,unreadMsgSnap)
                {
                  return Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(right: 10,top: 5),
                        child: SizedBox(
                          height: 40,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Text('Abc',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 11
                                ),
                                textAlign: TextAlign.start,),
                              Padding(
                                padding: const EdgeInsets.only(top: 5),
                                child:unreadMsgSnap.data!=null
                                ?unreadMsgSnap.data.docs.length>0
                                    ?Container(
                                  padding: EdgeInsets.all(3),
                                  decoration: BoxDecoration(
                                      color: Colors.red,
                                      borderRadius: BorderRadius.all(Radius.circular(20))
                                  ),
                                  child: Text(unreadMsgSnap.data.docs.length.toString(),
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 11
                                    ),
                                    textAlign: TextAlign.start,),
                                )
                                :Container()
                                :Container(),
                              )
                            ],
                          ),
                        ),
                      ),
                    ],
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
